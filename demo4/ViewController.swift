//
//  ViewController.swift
//  demo4
//
//  Created by Goutam on 8/28/19.
//  Copyright © 2019 Goutam. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myLabel: UILabel!
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "iOS" {
            myLabel.textColor = UIColor.blue
        } else {
            myLabel.textColor = UIColor.green
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

